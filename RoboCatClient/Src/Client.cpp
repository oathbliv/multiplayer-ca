
#include <RoboCatClientPCH.h>

bool Client::StaticInit( )
{
	ConnectionDetails::StaticInit();
	// Create the Client pointer first because it initializes SDL
	Client* client = new Client();

	if( WindowManager::StaticInit() == false )
	{
		return false;
	}
	
	if( GraphicsDriver::StaticInit( WindowManager::sInstance->GetMainWindow() ) == false )
	{
		return false;
	}
	HUD::StaticInit();

	TextureManager::StaticInit();

	RenderManager::StaticInit();
	Background::StaticInit();
	InputManager::StaticInit();


	sInstance.reset( client );

	return true;
}
//modified by andy
Client::Client()
{
	GameObjectRegistry::sInstance->RegisterCreationFunction('BKGD', Background::StaticCreate);
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'RCAT', RoboCatClient::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'MOUS', MouseClient::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'YARN', YarnClient::StaticCreate );

	string destination = ConnectionDetails::sInstance->GetClientDestination();
	string name = ConnectionDetails::sInstance->GetClientName();

	SocketAddressPtr serverAddress = SocketAddressFactory::CreateIPv4FromString( destination );

	NetworkManagerClient::StaticInit( *serverAddress, name );

	//NetworkManagerClient::sInstance->SetDropPacketChance( 0.6f );
	//NetworkManagerClient::sInstance->SetSimulatedLatency( 0.25f );
	//NetworkManagerClient::sInstance->SetSimulatedLatency( 0.5f );
	//NetworkManagerClient::sInstance->SetSimulatedLatency( 0.1f );
}



void Client::DoFrame()
{
		InputManager::sInstance->Update();

		Engine::DoFrame();

		NetworkManagerClient::sInstance->ProcessIncomingPackets();

		RenderManager::sInstance->Render();

		NetworkManagerClient::sInstance->SendOutgoingPackets();
}

void Client::HandleEvent( SDL_Event* inEvent )
{
	switch( inEvent->type )
	{
	case SDL_KEYDOWN:
		InputManager::sInstance->HandleInput( EIA_Pressed, inEvent->key.keysym.sym );
		break;
	case SDL_KEYUP:
		InputManager::sInstance->HandleInput( EIA_Released, inEvent->key.keysym.sym );
		break;
	// SDL Event for moving a joystick/analog on a controller
	case SDL_CONTROLLERBUTTONDOWN:
		InputManager::sInstance->HandleControllerInput(EIA_Pressed, inEvent);
		break;
	case SDL_CONTROLLERBUTTONUP:
		InputManager::sInstance->HandleControllerInput(EIA_Released, inEvent);
		break;
	// SDL Event for when a controller is hooked up
	case SDL_CONTROLLERDEVICEADDED:
		OutputDebugString("ADDING THE CONTROLLER");
		InputManager::sInstance->AddController(inEvent->cdevice.which);
	default:
		break;
	}
}

