//written by Andy
#include <RoboCatClientPCH.h>

std::unique_ptr< Background >	Background::sInstance;

Background::Background()
{
	mSpriteComponent.reset(new SpriteComponent(this));
	mSpriteComponent->SetTexture(TextureManager::sInstance->GetTexture("background"));
	mSpriteComponent->SetLayer(0);
}


void Background::StaticInit()
{
	sInstance.reset(new Background());
}