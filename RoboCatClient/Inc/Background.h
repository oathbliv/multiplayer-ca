//written by Andy
#include <SDL_ttf.h>

class Background : public GameObject
{
public:
	static void StaticInit();
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new Background()); }
	static std::unique_ptr< Background >	sInstance;

private:

	Background();
	SpriteComponentPtr	mSpriteComponent;
};

