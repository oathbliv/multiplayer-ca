// Written by: Ronan
//modified ShadowMultiplayer's ConnectionDetails
//Andy
class ConnectionDetails
{
public:
	static void StaticInit();
	static std::unique_ptr<ConnectionDetails> sInstance;


	ConnectionDetails();
	void Load();
	string GetClientName();
	string GetClientDestination();
	uint16_t GetServerPort();

private:
	string mClientName;
	string mClientIP;
	uint16_t mClientPort;
	uint16_t mServerPort;

};