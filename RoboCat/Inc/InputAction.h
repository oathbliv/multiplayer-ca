
enum EInputAction
{
	EIA_Pressed,
	EIA_Repeat,
	EIA_Released,
	EIA_ControllerAxis,
};