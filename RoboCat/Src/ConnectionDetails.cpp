#include "RoboCatPCH.h"

std::unique_ptr<ConnectionDetails> ConnectionDetails::sInstance;

void ConnectionDetails::StaticInit()
{
	ConnectionDetails* cd = new ConnectionDetails();
	cd->Load();
	sInstance.reset(cd);
}

ConnectionDetails::ConnectionDetails()
{
}

//modified by Andy
void ConnectionDetails::Load()
{
	uint16_t port = 45000;
	mClientPort = port;
	mServerPort = port;

	string line;

	std::ifstream detailsFile("../Assets/connection.txt");

	if (detailsFile.is_open())
	{
		for (int i = 0; i < 2; i++)
		{
			getline(detailsFile, line);
			if (i == 0)
			{
				mClientIP = line;
			}
			else
			{
				mClientName = line;
			}
		}
		detailsFile.close();
	}
}

string ConnectionDetails::GetClientName()
{
	return mClientName;
}

string ConnectionDetails::GetClientDestination()
{
	string asd = mClientIP + ":" + std::to_string(mClientPort);
	StringUtils::Log(asd.c_str(), 1);
	return mClientIP + ":" + std::to_string(mClientPort);
}

uint16_t ConnectionDetails::GetServerPort()
{
	return mServerPort;
}
