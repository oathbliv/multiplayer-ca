#include <RoboCatPCH.h>

//zoom hardcoded at 100...if we want to lock players on screen, this could be calculated from zoom
const float HALF_WORLD_HEIGHT = 3.6f;
const float HALF_WORLD_WIDTH = 6.4f;

RoboCat::RoboCat() :
	GameObject(),
	mMaxRotationSpeed(5.f),
	mMaxLinearSpeed(50.f),
	mVelocity(Vector3::Zero),
	mWallRestitution(0.1f),
	mCatRestitution(0.1f),
	mThrustDir(sf::Vector2f(0.f, 0.f)),
	mPlayerId(0),
	mIsShooting(false),
	mHealth(10),

	// Boolean to determine if this player has a boost available to them
	mbIsBoostAvailable(true),

	// Boolean to determine if this player is a seeker or not
	mbIsSeeker(false),

	// Boolean to determine if a player has just been tagged
	mbIsFrozen(false),

	// Unused for now
	mTaggedAt(0),

	// Max speed of the players who are not seeking
	mMaxLinearSpeedHider(30.f),
	rotation(0)
{
	SetCollisionRadius( 0.3f );
}

void RoboCat::ProcessInput( float inDeltaTime, const InputState& inInputState )
{
	//process our input....
	if (inInputState.IsBoosting() && mbIsBoostAvailable)
	{
		if (mBoostInt > 0)
			mBoostInt = mBoostInt - 1;
		else
			mbIsBoostAvailable = false;
	}
	
	//turning...
	int newRotation = 0;
	//float newRotation = GetRotation() + inInputState.GetDesiredHorizontalDelta() * mMaxRotationSpeed * inDeltaTime;
	//SetRotation( newRotation );

		//moving...
		//float inputForwardDelta = inInputState.GetDesiredVerticalDelta();
		//mThrustDir = inputForwardDelta;
		float inputHorizontalDelta = inInputState.GetDesiredHorizontalDelta();
		mThrustDir.x = inputHorizontalDelta;
		float inputForwardDelta = inInputState.GetDesiredVerticalDelta();
		mThrustDir.y = inputForwardDelta;



		if (mThrustDir.x == 0 && mThrustDir.y == 1)//north
		{
			rotation = 0;
			mDiagonal = false;
		}
		else if (mThrustDir.x == 1 && mThrustDir.y == 1)//northeast
		{
			rotation = 0.75f;
			mDiagonal = true;
		}
		else if (mThrustDir.x == 1 && mThrustDir.y == 0)//east
		{
			rotation = 1.5f;
			mDiagonal = false;
		}
		else if (mThrustDir.x == 1 && mThrustDir.y == -1)//southeast
		{
			rotation = 2.25f;
			mDiagonal = true;
		}
		else if (mThrustDir.x == 0 && mThrustDir.y == -1)//south
		{
			rotation = 3;
			mDiagonal = false;
		}
		else if (mThrustDir.x == -1 && mThrustDir.y == -1)//southwest
		{
			rotation = 3.75f;
			mDiagonal = true;
		}
		else if (mThrustDir.x == -1 && mThrustDir.y == 0)//west
		{
			rotation = 4.5f;
			mDiagonal = false;
		}
		else if (mThrustDir.x == -1 && mThrustDir.y == 1)//northwest
		{
			rotation = 5.25f;
			mDiagonal = true;
		}

		SetRotation(rotation);
}

// Sets player who gets tagged to be a seeker as a result of tag and gives the last seeker some points
void RoboCat::ProcessTag(RoboCat* collisionCat)
{
	if (!mbIsSeeker)
	{
		SetSeeker(true);
	}

	
	// Set old seeker to be normal again
	collisionCat->SetSeeker(false);

	// Flag the player as tagged to be respawned later
	mbIsFrozen = true;

	// Give the seeker 5 points for tagging someone
	if (ScoreBoardManager::sInstance->GetTimer() <= 0)
	{
		ScoreBoardManager::sInstance->IncScore(collisionCat->mPlayerId, 5);
	}

	//collisionCat->mbIsFrozen = true;
	//SetDoesWantToDie(true);
}

// Unused function for attempt to 'freeze' player when tagged
void RoboCat::DecrementFrozenTimer()
{
	mTaggedAt -= Timing::sInstance.GetDeltaTime();
}

// Setter for mbIsSeeker
void RoboCat::SetSeeker(bool newState)
{
	mbIsSeeker = newState;
	
}

// Getter for mbIsFrozen
bool RoboCat::GetFrozen()
{
	return mbIsFrozen;
}

// Getter for mTaggedAt (unused)
float RoboCat::GetTaggedAt()
{
	return mTaggedAt;
}

// Setter for mbIsFrozen
void RoboCat::SetFrozen(bool newState)
{
	mbIsFrozen = newState;
}

// Getter for mbIsSeeker
bool RoboCat::GetSeeker()
{
	return mbIsSeeker;
}

void RoboCat::AdjustVelocityByThrust( float inDeltaTime )
{

		//just set the velocity based on the thrust direction -- no thrust will lead to 0 velocity
		//simulating acceleration makes the client prediction a bit more complex
		Vector3 forwardVector = GetForwardVector();
		//mVelocity = forwardVector * ( mThrustDir * inDeltaTime * mMaxLinearSpeed );

		
		// Use the correct speed for if the player is seeking or not
		if (mbIsSeeker)
		{
			mVelocity.mX = mThrustDir.x * inDeltaTime * mMaxLinearSpeed;
			mVelocity.mY = -mThrustDir.y * inDeltaTime * mMaxLinearSpeed;
			if (mDiagonal == true)
			{
				mVelocity = Vector3(mVelocity.mX / 1.4142136f, mVelocity.mY / 1.4142136f, mVelocity.mZ);
			}
		}
		else
		{
			mVelocity.mX = mThrustDir.x * inDeltaTime * mMaxLinearSpeedHider;
			mVelocity.mY = -mThrustDir.y * inDeltaTime * mMaxLinearSpeedHider;
			if (mDiagonal == true)
			{
				mVelocity = Vector3(mVelocity.mX / 1.4142136f, mVelocity.mY / 1.4142136f, mVelocity.mZ);
			}
		}
}

void RoboCat::SimulateMovement( float inDeltaTime )
{
	//simulate us...
	AdjustVelocityByThrust(inDeltaTime);

	SetLocation(GetLocation() + mVelocity * inDeltaTime);
	ProcessCollisions();
}

void RoboCat::Update()
{
	// If the player was tagged then we want to respawn them as the seeker
	if (mbIsFrozen)
		SetDoesWantToDie(true);
}

void RoboCat::ProcessCollisions()
{
	//right now just bounce off the sides..
	ProcessCollisionsWithScreenWalls();

	float sourceRadius = GetCollisionRadius();
	Vector3 sourceLocation = GetLocation();

	//now let's iterate through the world and see what we hit...
	//note: since there's a small number of objects in our game, this is fine.
	//but in a real game, brute-force checking collisions against every other object is not efficient.
	//it would be preferable to use a quad tree or some other structure to minimize the
	//number of collisions that need to be tested.
	for( auto goIt = World::sInstance->GetGameObjects().begin(), end = World::sInstance->GetGameObjects().end(); goIt != end; ++goIt )
	{
		GameObject* target = goIt->get();
		if( target != this && !target->DoesWantToDie() && target->isCollidable() )
		{
			//simple collision test for spheres- are the radii summed less than the distance?
			Vector3 targetLocation = target->GetLocation();
			float targetRadius = target->GetCollisionRadius();

			Vector3 delta = targetLocation - sourceLocation;
			float distSq = delta.LengthSq2D();
			float collisionDist = ( sourceRadius + targetRadius );
			if( distSq < ( collisionDist * collisionDist ) )
			{
				//first, tell the other guy there was a collision with a cat, so it can do something...

				if( target->HandleCollisionWithCat( this ) )
				{
					//okay, you hit something!
					//so, project your location far enough that you're not colliding
					Vector3 dirToTarget = delta;
					dirToTarget.Normalize2D();
					Vector3 acceptableDeltaFromSourceToTarget = dirToTarget * collisionDist;
					//important note- we only move this cat. the other cat can take care of moving itself
					SetLocation( targetLocation - acceptableDeltaFromSourceToTarget );

					Vector3 relVel = mVelocity;
				
					//if other object is a cat, it might have velocity, so there might be relative velocity...
					RoboCat* targetCat = target->GetAsCat();
					
					if( targetCat )
					{
						relVel -= targetCat->mVelocity;

						// If we're seeking and collide with another cat we need to carry out the tagging process 
						if(this->mbIsSeeker)
							targetCat->ProcessTag(this);
					}

					//got vel with dir between objects to figure out if they're moving towards each other
					//and if so, the magnitude of the impulse ( since they're both just balls )
					float relVelDotDir = Dot2D( relVel, dirToTarget );

					if (relVelDotDir > 0.f)
					{
						Vector3 impulse = relVelDotDir * dirToTarget;
					
						if( targetCat )
						{
							mVelocity -= impulse;
							mVelocity *= mCatRestitution;
						}
						else
						{
							mVelocity -= impulse * 2.f;
							mVelocity *= mWallRestitution;
						}

					}
				}
			}
		}
	}

}

void RoboCat::ProcessCollisionsWithScreenWalls()
{
	Vector3 location = GetLocation();
	float x = location.mX;
	float y = location.mY;

	float vx = mVelocity.mX;
	float vy = mVelocity.mY;

	float radius = GetCollisionRadius();

	//if the cat collides against a wall, the quick solution is to push it off
	if( ( y + radius ) >= HALF_WORLD_HEIGHT && vy > 0 )
	{
		
		mVelocity.mY = -vy * mWallRestitution;
		location.mY = HALF_WORLD_HEIGHT - radius;
		SetLocation( location );
	}
	else if( y <= ( -HALF_WORLD_HEIGHT + radius ) && vy < 0 )
	{
		mVelocity.mY = -vy * mWallRestitution;
		location.mY = -HALF_WORLD_HEIGHT + radius;
		SetLocation( location );
	}

	if( ( x + radius ) >= HALF_WORLD_WIDTH && vx > 0 )
	{
		mVelocity.mX = -vx * mWallRestitution;
		location.mX = HALF_WORLD_WIDTH - radius;
		SetLocation( location );
	}
	else if(  x <= ( -HALF_WORLD_WIDTH + radius ) && vx < 0 )
	{
		mVelocity.mX = -vx * mWallRestitution;
		location.mX = -HALF_WORLD_WIDTH + radius;
		SetLocation( location );
	}
}

uint32_t RoboCat::Write( OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState ) const
{
	uint32_t writtenState = 0;

	if( inDirtyState & ECRS_PlayerId )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write( GetPlayerId() );

		writtenState |= ECRS_PlayerId;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}


	if( inDirtyState & ECRS_Pose )
	{
		inOutputStream.Write( (bool)true );

		Vector3 velocity = mVelocity;
		inOutputStream.Write( velocity.mX );
		inOutputStream.Write( velocity.mY );

		Vector3 location = GetLocation();
		inOutputStream.Write( location.mX );
		inOutputStream.Write( location.mY );

		inOutputStream.Write( GetRotation() );

		writtenState |= ECRS_Pose;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	//always write mThrustDir- it's just two bits
	if( mThrustDir != sf::Vector2f(0.f,0.f) )
	{
		inOutputStream.Write( true );
		inOutputStream.Write( mThrustDir.x > 0.f );
		inOutputStream.Write(mThrustDir.y > 0.f);
	}
	else
	{
		inOutputStream.Write( false );
	}

	if( inDirtyState & ECRS_Color )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write( GetColor() );

		writtenState |= ECRS_Color;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	if( inDirtyState & ECRS_Health )
	{
		inOutputStream.Write( (bool)true );
		inOutputStream.Write( mHealth, 4 );

		writtenState |= ECRS_Health;
	}
	else
	{
		inOutputStream.Write( (bool)false );
	}

	if (inDirtyState & ECRS_SEEK)
	{
		inOutputStream.Write((bool)true);
		inOutputStream.Write(mbIsSeeker);

		writtenState |= ECRS_SEEK;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}
	


	inOutputStream.Write(mbIsSeeker);
	

	return writtenState;
	

}


