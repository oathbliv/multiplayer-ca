
#include <RoboCatServerPCH.h>


//uncomment this when you begin working on the server

bool Server::StaticInit()
{
	ConnectionDetails::StaticInit();
	
	sInstance.reset( new Server() );

	return true;
}

Server::Server()
{

	GameObjectRegistry::sInstance->RegisterCreationFunction( 'RCAT', RoboCatServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'MOUS', MouseServer::StaticCreate );
	GameObjectRegistry::sInstance->RegisterCreationFunction( 'YARN', YarnServer::StaticCreate );
	
	// Initial time before spawning the first wave of coins
	mSpawnDelay = 10;

	// Unused for countdown timer
	mRoundTimer = 30;
	mCountdown = true;

	InitNetworkManager();
	
	//NetworkManagerServer::sInstance->SetDropPacketChance( 0.8f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.25f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.5f );
	//NetworkManagerServer::sInstance->SetSimulatedLatency( 0.1f );

}


int Server::Run()
{
	SetupWorld();
	
	return Engine::Run();
}

bool Server::InitNetworkManager()
{
	uint16_t port = ConnectionDetails::sInstance->GetServerPort();
	return NetworkManagerServer::StaticInit(port);
}


namespace
{
	
	void CreateRandomMice( int inMouseCount )
	{
		Vector3 mouseMin( -5.f, -3.f, 0.f );
		Vector3 mouseMax( 5.f, 3.f, 0.f );
		GameObjectPtr go;

		//make a mouse somewhere- where will these come from?
		for( int i = 0; i < inMouseCount; ++i )
		{
			go = GameObjectRegistry::sInstance->CreateGameObject( 'MOUS' );
			Vector3 mouseLocation = RoboMath::GetRandomVector( mouseMin, mouseMax );
			go->SetLocation( mouseLocation );
		}
	}
}

void Server::SetupWorld()
{
	
}

// Function to set the time between each wave of coins spawning
void Server::SetSpawnCoins()
{
	// hardcoded delay of 7s between the spawning of coins
	mSpawnDelay = Timing::sInstance.GetFrameStartTime() + 7;
}

// Function to carry out the spawning of coins
void Server::HandleSpawning(int numberToSpawn)
{
	if (ScoreBoardManager::sInstance->GetCoinCount() <= 3)
	{
		// If we've elapsed enough time since the last spawn
		if (mSpawnDelay != 0.f && Timing::sInstance.GetFrameStartTime() > mSpawnDelay)
		{
			// Bounds for spawning coins
			// -6 -> 6 X values of whole screen
			// -3 -> 3 Y values of whole screen
			Vector3 coinMin(-6.f, -3.f, 0.f);
			Vector3 coinMax(6.f, 3.f, 0.f);
			GameObjectPtr go;

			// For each coin we want to spawn
			for (int i = 0; i < numberToSpawn; ++i)
			{
				// Create the coin and set it's location to a random point on the screen
				go = GameObjectRegistry::sInstance->CreateGameObject('MOUS');
				Vector3 coinLoc = RoboMath::GetRandomVector(coinMin, coinMax);
				go->SetLocation(coinLoc);
			}

			// Reset the delay for safety to ensure this function doesn't fire multiple times accidentally
			mSpawnDelay = 0.f;

			// Reset the delay to the appropriate time between waves
			SetSpawnCoins();
		}
		ScoreBoardManager::sInstance->SetCoinCount(ScoreBoardManager::sInstance->GetCoinCount() + numberToSpawn);
	}
}


void Server::DoFrame()
{
	NetworkManagerServer::sInstance->ProcessIncomingPackets();

	NetworkManagerServer::sInstance->CheckForDisconnects();

	NetworkManagerServer::sInstance->RespawnCats();

	// Do the check for if we need to spawn the coins, hardcoded 15 coins for now
	if (mCountdown == false)
	{
		HandleSpawning(10);
	}

	Engine::DoFrame();

	Countdown();

	NetworkManagerServer::sInstance->SendOutgoingPackets();

}

void Server::HandleNewClient( ClientProxyPtr inClientProxy )
{
	int playerId = inClientProxy->GetPlayerId();
	
	ScoreBoardManager::sInstance->AddEntry( playerId, inClientProxy->GetName() );
	SpawnCatForPlayer( playerId );
}

// Default function for spawning players
void Server::SpawnCatForPlayer( int inPlayerId )
{
	RoboCatPtr cat = std::static_pointer_cast< RoboCat >( GameObjectRegistry::sInstance->CreateGameObject( 'RCAT' ) );
	cat->SetColor( ScoreBoardManager::sInstance->GetEntry( inPlayerId )->GetColor() );
	cat->SetPlayerId( inPlayerId );

	// The first player to join the server will be seeking to begin with
	if (inPlayerId == 1)
		cat->SetSeeker(true);

	Vector3 spawnMin(-6.f, -3.f, 0.f);
	Vector3 spawnMax(6.f, 3.f, 0.f);

	if (cat->GetSeeker())
	{
		cat->SetColor(Colors::Red);
		cat->SetLocation(Vector3(0.f, 0.f, 0.f));
	}
	else
		cat->SetLocation(RoboMath::GetRandomVector(spawnMin, spawnMax));
}

// Function called upon respawn of players when they become seekers
void Server::SpawnCatAsSeekerForPlayer(int inPlayerId)
{
	RoboCatPtr cat = std::static_pointer_cast< RoboCat >(GameObjectRegistry::sInstance->CreateGameObject('RCAT'));
	//cat->SetColor(ScoreBoardManager::sInstance->GetEntry(inPlayerId)->GetColor());
	cat->SetPlayerId(inPlayerId);
	cat->SetSeeker(true);
	cat->SetColor(Colors::Red);
	// Spawn location after respawn set to bottom right corner
	cat->SetLocation(Vector3(6.f, 3.f, 0.f));
}

void Server::HandleLostClient( ClientProxyPtr inClientProxy )
{
	//kill client's cat
	//remove client from scoreboard
	int playerId = inClientProxy->GetPlayerId();

	ScoreBoardManager::sInstance->RemoveEntry( playerId );
	RoboCatPtr cat = GetCatForPlayer( playerId );
	if( cat )
	{
		cat->SetDoesWantToDie( true );
	}
}

RoboCatPtr Server::GetCatForPlayer( int inPlayerId )
{
	//run through the objects till we find the cat...
	//it would be nice if we kept a pointer to the cat on the clientproxy
	//but then we'd have to clean it up when the cat died, etc.
	//this will work for now until it's a perf issue
	const auto& gameObjects = World::sInstance->GetGameObjects();
	for( int i = 0, c = gameObjects.size(); i < c; ++i )
	{
		GameObjectPtr go = gameObjects[ i ];
		RoboCat* cat = go->GetAsCat();
		if( cat && cat->GetPlayerId() == inPlayerId )
		{
			return std::static_pointer_cast< RoboCat >( go );
		}
	}

	return nullptr;

}
//Andy
void Server::Countdown()
{
	if (!mIsBegun && ScoreBoardManager::sInstance->GetEntries().size() >= 4)
	{
		mIsBegun = true;
		beginTime = Timing::sInstance.GetFrameStartTime();
	}

	if (mCountdown == true && ScoreBoardManager::sInstance->GetEntries().size() >= 4)
	{
		time = Timing::sInstance.GetFrameStartTime() - beginTime;

		//minus the countdown time
		ScoreBoardManager::sInstance->SetTimer(mRoundTimer - (int)time);

		if (ScoreBoardManager::sInstance->GetTimer() <= 0)
		{
			//allow game to start
			mCountdown = false;
		}
	}
}