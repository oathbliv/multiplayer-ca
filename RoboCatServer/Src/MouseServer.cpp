#include <RoboCatServerPCH.h>

MouseServer::MouseServer()
{
}

void MouseServer::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject( this );
}


bool MouseServer::HandleCollisionWithCat( RoboCat* inCat )
{
	if (!inCat->GetSeeker())
	{
		//kill yourself!
		SetDoesWantToDie(true);

		ScoreBoardManager::sInstance->IncScore(inCat->GetPlayerId(), 1);
		ScoreBoardManager::sInstance->MinusCoinCount();
	}

	return false;
}


