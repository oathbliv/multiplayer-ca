#include <RoboCatServerPCH.h>

namespace
{
	const float kRespawnDelay = 1.5f;
}

ClientProxy::ClientProxy( const SocketAddress& inSocketAddress, const string& inName, int inPlayerId ) :
mSocketAddress( inSocketAddress ),
mName( inName ),
mPlayerId( inPlayerId ),
mDeliveryNotificationManager( false, true ),
mIsLastMoveTimestampDirty( false ),
mTimeToRespawn( 0.f )
{
	UpdateLastPacketTime();
}


void ClientProxy::UpdateLastPacketTime()
{
	mLastPacketFromClientTime = Timing::sInstance.GetTimef(); 
}

void	ClientProxy::HandleCatDied()
{
	mTimeToRespawn = Timing::sInstance.GetFrameStartTime() + kRespawnDelay;
}

void	ClientProxy::RespawnCatIfNecessary()
{
	if( mTimeToRespawn != 0.f && Timing::sInstance.GetFrameStartTime() > mTimeToRespawn )
	{
		// If we're respawning a player it's because they were recently tagged, so let's spawn them as the seeker
		static_cast< Server* > ( Engine::sInstance.get() )->SpawnCatAsSeekerForPlayer( mPlayerId );

		mTimeToRespawn = 0.f;
	}
}
