class Server : public Engine
{
public:

	static bool StaticInit();

	virtual void DoFrame() override;

	virtual int Run();

	void HandleNewClient( ClientProxyPtr inClientProxy );
	void HandleLostClient( ClientProxyPtr inClientProxy );

	RoboCatPtr	GetCatForPlayer( int inPlayerId );
	void	SpawnCatForPlayer( int inPlayerId );

	void SpawnCatAsSeekerForPlayer(int inPlayerId);
	void SetSpawnCoins();
	void HandleSpawning(int numberToSpawn);
	void Countdown();

	float mSpawnDelay;
	sf::Clock mClock;
	int mRoundTimer;
	bool mCountdown;
	float time;
	bool mIsBegun = false;
	float beginTime;

private:
	Server();

	bool	InitNetworkManager();
	void	SetupWorld();


	

};